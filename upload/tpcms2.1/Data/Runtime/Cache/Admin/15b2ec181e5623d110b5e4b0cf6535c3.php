<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<link href="/dwz/./Core/Tpcms/Admin/View/Public/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" />
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
		<ul class="cc">
			<li class="current"><a href="javascrip:;">栏目列表</a></li>
			<li ><a href="<?php echo U('Category/add');?>">添加栏目</a></li>
			<li ><a href="<?php echo U('Category/update_cache');?>">更新栏目缓存</a></li>
		</ul>
	</div>
  <form name="myform" action="<?php echo U('Category/sort');?>" method="post" class="J_ajaxForm">
  <div class="table_list">
    <table width="100%">
        <colgroup>
	        <col width="50">
	        <col width="55">
	      
	        <col>
	        <col width="80">
	        <col width="100">
	        <col width="50" >
	        <col width="300">
        </colgroup>
        <thead>
          <tr>
          	<td>Cid</td>
            <td align='center'>排序</td>
            <td>栏目名称</td>
            <td align='center'>栏目类型</td>
            <td>所属模型</td>
            <td align='center'>访问</td>
            <td align='center'>管理操作</td>
          </tr>
        </thead>
        <?php if($category): if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr cid='<?php echo ($value["cid"]); ?>' <?php if($value['pid'] == 0): ?>class="top"<?php endif; ?> >
        	<td> <?php echo ($value["cid"]); ?></td>
    			 <td align='center'>
    				<input name='sort[<?php echo ($value["cid"]); ?>]' type='text' size='3' value='<?php echo ($value["sort"]); ?>' class='input'>
    			</td>
  			
			   <td >
				
				<?php if($value['pid'] == 0 && Third\Data::hasChild(S('category'),$value['cid'])): ?><img src="/dwz/./Core/Tpcms/Admin/View/Public/images/contract.gif" action="2" class="explodeCategory hand"/><?php endif; ?>
				<?php if($value['pid'] == 0): ?><strong><?php echo ($value["_name"]); ?></strong>
				<?php else: ?>
					<?php echo ($value["_name"]); endif; ?>

			</td>
			<td  align='center'><?php echo ($value["type"]); ?></td>
			<td><?php echo ($value["model"]); ?></td>
			<td align='center'><a href='<?php if($value['cat_type'] == 4): echo U('/'.strtolower($value['remark'])."_v_".$value['cid']); else: echo U('/'.strtolower($value['remark'])."_l_".$value['cid']); endif; ?>' target='_blank'>访问</a></td>
			<td align='center' >
				<a href="<?php echo U('Category/add',array('pid'=>$value['cid']));?>">添加子栏目</a> | 
				<a href="<?php echo U('Category/edit',array('cid'=>$value['cid']));?>">修改</a> | 
				<a class="J_ajax_del" href="<?php echo U('Category/del',array('cid'=>$value['cid']));?>">删除</a>  
			</td>
		</tr><?php endforeach; endif; else: echo "" ;endif; ?>
    <?php else: ?>
        <tr>
          <td colspan="7">没有找到任何符合条件记录</td>
        </tr><?php endif; ?>
	</table>
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">排序</button>
      </div>
    </div>
  </div>

</form>
</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>
<script>
var PUBLIC = '/dwz/./Core/Tpcms/Admin/View/Public';
$(function(){


  var show =[];
  //展开栏目
  $(".explodeCategory").click(function () {
      var action = parseInt($(this).attr("action"));
      var tr = $(this).parents('tr').eq(0);
      var cid = tr.attr("cid");
      show[cid] = {'cid':cid,'action':action};
      switch (action) {
          case 1://展示
              $(tr).nextUntil('.top').show();
              $(this).attr('action', 2);
              $(this).attr('src', PUBLIC+"/images/contract.gif");
              break;
          case 2://收缩
              $(tr).nextUntil('.top').hide();
              $(this).attr('action', 1);
              $(this).attr('src', PUBLIC+"/images/explode.gif");
              break;
      }
      setCookie('categoryShow',JSON.stringify(show));
  })

  // 初始化菜单
  explodeCategory();
  function explodeCategory()
  {
    var cookieshow = getCookie('categoryShow');
    cookieshow = $.parseJSON(cookieshow);
    $(".explodeCategory").each(function () 
    {
        var action = parseInt($(this).attr("action"));
        var tr = $(this).parents('tr').eq(0);
        var cid = tr.attr("cid");
        if(cookieshow)
        {
            $.each(cookieshow,function(k,v){
              if(JSON.stringify(v) != 'null')
              {
                  if(v.cid==cid)
                  {
                    action = v.action;
                    return ;
                  }
              }
          })
        }
        
        show[cid] = {'cid':cid,'action':action};
        switch (action) 
        {
            case 1://展示
                $(tr).nextUntil('.top').show();
                $(this).attr('action', 2);
                $(this).attr('src', PUBLIC+"/images/contract.gif");
                break;
            case 2://收缩
                $(tr).nextUntil('.top').hide();
                $(this).attr('action', 1);
                $(this).attr('src', PUBLIC+"/images/explode.gif");
                break;
        }
        setCookie('categoryShow',JSON.stringify(show));
    })

  }

})


</script>
</body>
</html>