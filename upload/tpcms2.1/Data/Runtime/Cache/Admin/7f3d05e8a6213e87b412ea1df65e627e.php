<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<link href="/dwz/./Core/Tpcms/Admin/View/Public/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" />
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
	<div class="nav">
		<ul class="cc">
			<li class="current"><a href="javascrip:;">广告位置</a></li>
			<li ><a href="<?php echo U('Position/add');?>">添加广告位置</a></li>
			<li ><a href="<?php echo U('Position/update_cache');?>">更新广告位置缓存</a></li>
		</ul>
	</div>
  <form name="myform" action="<?php echo U('Position/sort');?>" method="post" class="J_ajaxForm">
  <div class="table_list">
    <table width="100%">
        <colgroup>
	        <col width="38">
	        
	        <col>
	        <col width="100">
	        <col width="100" >
	        <col width="300">
        </colgroup>
        <thead>
          <tr>
          	<td>id</td>
            
            <td>位置名称</td>
            <td>宽度(px)</td>
            <td>高度(px)</td>
            <td align='center'>管理操作</td>
          </tr>
        </thead>

        <?php if($data): if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$value): $mod = ($i % 2 );++$i;?><tr>
        	<td>
				<?php echo ($value["psid"]); ?>
			</td>
			<td ><?php echo ($value["position_name"]); ?></td>
			<td ><?php echo ($value["width"]); ?></td>
			<td ><?php echo ($value["height"]); ?></td>
			<td align='center' >
				<a href="<?php echo U('Position/edit',array('psid'=>$value['psid']));?>">修改</a> | 
				<a class="J_ajax_del" href="<?php echo U('Position/del',array('psid'=>$value['psid']));?>">删除</a>  
			</td>
		</tr><?php endforeach; endif; else: echo "" ;endif; ?>
		<?php else: ?>
		<tr>
			<td colspan="5">没有找到符合条件的记录</td>
		</tr><?php endif; ?>
	</table>
<!--     <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <button class="btn btn_submit mr10 J_ajax_submit_btn" type="submit">排序</button>
      </div>
    </div> -->
  </div>

</form>
</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>
</body>
</html>