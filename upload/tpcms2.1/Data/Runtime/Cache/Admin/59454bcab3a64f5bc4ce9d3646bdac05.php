<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<link href="/dwz/./Core/Tpcms/Admin/View/Public/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" />
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<style>
.pop_nav{
	padding: 0px;
}
.pop_nav ul{
	border-bottom:1px solid #266AAE;
	padding:0 5px;
	height:25px;
	clear:both;
}
.pop_nav ul li.current a{
	border:1px solid #266AAE;
	border-bottom:0 none;
	color:#333;
	font-weight:700;
	background:#F3F3F3;
	position:relative;
	border-radius:2px;
	margin-bottom:-1px;
}

</style>
<div class="wrap J_check_wrap">
  <div class="nav">
    <ul class="cc">
		<li class="current"><a href="javascrip:;">网站设置</a></li>
		<li ><a href="<?php echo U('Config/add');?>">添加配置</a></li>
		<li ><a href="<?php echo U('Config/update_cache');?>">更新缓存</a></li>
      </ul>
	</div>
  <div class="pop_nav">
    <ul class="J_tabs_nav">
	  <?php if(is_array($data)): foreach($data as $key=>$v): ?><li <?php if($key == '基本设置'): ?>class='current'<?php endif; ?>><a href="javascript:;;"><?php echo ($key); ?></a></li><?php endforeach; endif; ?>
    </ul>
  </div>
  <form method="post" class="J_ajaxForm" action="<?php echo U('Config/edit');?>" >
    <div class="J_tabs_contents">
    	<?php if(is_array($data)): foreach($data as $key=>$v): ?><div <?php if($key != '基本设置'): ?>style='display:none'<?php endif; ?>>
	        <div class="h_a"><?php echo ($key); ?></div>
	        <div class="table_full">
	          <table width="100%" class="table_form ">

	           <tbody>
					<?php if(is_array($v)): foreach($v as $key=>$value): ?><tr>
							<th width="120"><input type="text" name="sort[<?php echo ($value["code"]); ?>]" value="<?php echo ($value["sort"]); ?>" class='input length_2 '/></th>
							<th width="200"><?php echo ($value["title"]); ?> <br><?php echo ($value["code"]); ?></th>
							<td align="left"><?php echo ($value["form"]); ?></td>
				
						</tr><?php endforeach; endif; ?>
				</tbody>
	          </table>
	        </div>
	      </div><?php endforeach; endif; ?>
    
      
      </div>
 
    <div class="btn_wrap">
      <div class="btn_wrap_pd">
        <button class="btn btn_submit mr10 J_ajax_submit_btn " type="submit">提交</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript" src="/dwz/Core/Org/common.js"></script>
<script type="text/javascript" src="/dwz/Core/Org/content_addtop.js"></script>
</body>
</html>