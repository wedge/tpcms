<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<link href="/dwz/./Core/Tpcms/Admin/View/Public/css/admin_style.css" rel="stylesheet" />
<link href="/dwz/Core/Org/artDialog/skins/default.css" rel="stylesheet" />
<script type="text/javascript">
//全局变量
var GV = {
    DIMAUB: "/dwz/",
	JS_ROOT: "/dwz/Core/Org/"
};
</script>
<script src="/dwz/Core/Org/wind.js"></script>
<script src="/dwz/Core/Org/jquery.js"></script>
</head>
<style type="text/css">
 html{font-size:62.5%;font-family:Tahoma}
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td,hr{margin:0;padding:0}
body{line-height:1.333;font-size:12px;font-size:1.2rem}
h1,h2,h3,h4,h5,h6{font-size:100%}
input,textarea,select,button{font-size:12px;font-weight:normal}
input[type="button"],input[type="submit"],select,button{cursor:pointer}
table{border-collapse:collapse;border-spacing:0}
address,caption,cite,code,dfn,em,th,var{font-style:normal;font-weight:normal}
li{list-style:none}
caption,th{text-align:left}
q:before,q:after{content:''}
abbr,acronym{border:0;font-variant:normal}
sup{vertical-align:text-top}
sub{vertical-align:text-bottom}
fieldset,img,a img,iframe{border-width:0;border-style:none}
iframe{overflow:hidden}
img{ -ms-interpolation-mode:bicubic;}
textarea{overflow-y:auto}
legend{color:#000}
a:link,a:visited{text-decoration:none}
hr{height:0}
label{cursor:pointer}
.os_winXp{font-family:Tahoma}
.os_mac{font-family:"Helvetica Neue",Helvetica,"Hiragino Sans GB",Arial}
.os_vista,.os_win7{font-family:"Microsoft Yahei",Tahoma}
.clearfix:before,.clearfix:after{content:".";display:block;height:0;visibility:hidden}
.clearfix:after{clear:both}
.clearfix{zoom:1}
.header,nav,.footer{display:block}
body{background-color:#f0f0f0}
.wrap{background-color:#f5f5f5}
.wrap .inner{width:500px;margin:0 auto}
iframe{background-color:transparent}
/*.header{padding:19px 0 0 100px}*/
.header h1{ background-image:url(/dwz/./Core/Tpcms/Admin/View/Public/images/logo.png);background-repeat:no-repeat;width:100px;height:100px;line-height:150px;overflow:hidden;font-size:0; margin: 0 auto}
.qzone_login{margin-top:55px}
.qzone_login .qzone_cont{float:left;margin-left:112px;position:relative;width:429px;_display:inline;overflow:hidden;height:321px}
.qzone_cont .img_list{width:429px;height:321px}
.qzone_cont .img_list li{width:429px;height:321px;vertical-align:middle;display:table-cell}
.qzone_cont .img_list .img_link{display:block;width:429px;text-align:center;height:321px;outline:none;overflow:hidden}
.qzone_cont .scroll_img_box{margin:40px auto 0;height:16px;float:left}
.qzone_cont .scroll_img{text-align:center;width:429px}
.qzone_cont .scroll_img li{ width:10px;height:10px;background-image:url(/dwz/./Core/Tpcms/Admin/View/Public/images/qzone_login.png);background-position:-663px 0;background-repeat:no-repeat;display:inline-block;margin-right:15px;cursor:pointer;*display:inline;*zoom:1;overflow:hidden}
.qzone_cont .scroll_img .current_img{ background-image:url(/dwz/./Core/Tpcms/Admin/View/Public/images/admin_img/qzone_login.png);background-position:-663px -17px}
.qzone_login .login_main{margin:0 auto;width:390px;overflow:hidden}
.qzone_login .login_main a{color:#3da5dc}
.login_main .login_list .input_txt{border:1px solid #54b4e8;border-radius:3px;font-size:16px;font-family:"Microsoft Yahei",Tahoma;height:23px;width:379px;color:#666;padding:14px 0 14px 9px;margin-bottom:20px}
.login_main .login_list .input_txt:focus{outline:0}
.login_main .login_list .current_input{border-color:#56bdf3;box-shadow:inset 0 1px 3px rgba(0,0,0,.2);-webkit-box-shadow:inset 0 1px 3px rgba(0,0,0,.2);-moz-box-shadow:inset 0 1px 3px rgba(0,0,0,.2)}
.login_main .login_list .login_input{position:relative;width:390px;height:73px}
.login_main .login_list .txt_default{position:absolute;font-size:16px;font-family:"Microsoft Yahei",Tahoma;color:#666;top:17px;left:10px;cursor:text}
.login_main .login_list .txt_click{color:#ccc}
.login_main .login_list .yanzhengma{position:relative;color:#666}
.login_main .login_list .yanzhengma .yanzheng_txt{margin-left:2px}
.login_main .login_list .yanzhengma .input_txt{width:230px;margin-bottom:40px}
.login_main .login_list .yanzhengma .yanzhengma_box{position:absolute;left:265px;top:0}
.login_main .login_list .yanzhengma .yanzheng_img{display:block;margin-bottom:10px}
.login_main .login_btn{ width:390px;height:48px;line-height:48px;overflow:hidden;font-size:20px;*background:none;/*background-image:url(/dwz/./Core/Tpcms/Admin/View/Public/images/qzone_login.png);*/background-position:-514px 0;border:none;cursor:pointer;background: #54b4e8;color:#fff;}
.qzone_login .login_main nav{color:#d0d3d7;margin:20px 0 0 3px}
.qzone_login .login_main nav .sep{margin:0 12px}
.login_main .quick_login{color:#5a5b5b}
.login_main .wrong_notice{color:red;margin:0 0 10px 1px}
.login_main .login_change{margin:6px 0 0 3px}
.platform_box{margin:94px 0 0 0;width:1000px;padding-bottom:16px}
.platform_box nav{ background-image:url(/dwz/./Core/Tpcms/Admin/View/Public/images/qzone_login.png);background-position:0 0;background-repeat:no-repeat;width:390px;height:52px;margin:0 auto}
.platform_box nav .platform_link{width:86px;margin:0 1px;height:52px;line-height:160px;overflow:hidden;display:inline-block;font-size:0;*margin-top:-64px}
.footer{ background:#f0f0f0 url(/dwz/./Core/Tpcms/Admin/View/Public/images/ft_bg.jpg) repeat-x;color:#999}
.footer .inner{width:1000px;margin:0 auto;text-align:center;padding:45px 0}
.footer .links{margin-bottom:15px}
.footer .links .sep{margin:0 12px;color:#d0d3d7}
.footer .copyright{width:580px;margin:0 auto}
.footer .copyright_en{float:left;margin-right:15px}
.footer .copyright_ch{float:left}
.footer .copyright_ch .copyright_link{margin-left:5px}


</style>
<script type="text/javascript">
if (window.parent !== window.self) {
	document.write = '';
	window.parent.location.href = window.self.location.href;
	setTimeout(function () {
		document.body.innerHTML = '';
	}, 0);
}
</script>
</head>
<body>
<div class="wrap">
	<div class="inner" >
		<div class="header">
			<h1>ShuipFCMS内容管理系统</h1>
		</div>
		<div class="qzone_login clearfix">
			<!-- <div class="qzone_cont" id="_pt">
				<li>
					<img src="/dwz/./Core/Tpcms/Admin/View/Public/images/login_bg.jpg" alt="生活以快乐为基准，爱情以互惠为原则！"></li>
			</div> -->
			<!-- end qzone_cont -->
			<div class="login_main">
				
				<form id="loginform" method="post" name="loginform" action="<?php echo U('index');?>"   >
					<ul class="login_list"  id="web_login">
						<li class="login_input">
							<input  value=""  id="username" name="username"  class="input_txt" tabindex="1"   type="text" value="" placeholder="帐号名" title="帐号名"  />
						</li>
						<li class="login_input">
							<input maxlength=16 type="password"  id="password" name="password" tabindex="2"   class="input_txt"  value=""  placeholder="密码" title="密码"/>
						</li>
						<li class="yanzhengma clearfix" id="verifytip" style="display:none">
							<span id="verifyinput">
								<input  id="verifycode" name="code" maxlength=5 tabindex="3" class="input_txt" type="text" value=""  placeholder="请输入验证码" />
							</span>
							<div class="yanzhengma_box" id="verifyshow">
								<img class="yanzheng_img" id="code_img" alt="" src="<?php echo U('verify');?>">
								<a href="javascript:;;" onClick="refreshs()" class="change_img">看不清，换一张</a>
							</div>
						</li>
						<li>

							<button type="submit" class="login_btn" tabindex="4" id="subbtn">登录</button>
						</li>
					</ul>
				</form>
				<div class="quick_login" id="qlogin"></div>
			</div>
		</div>
		<div class="platform_box"></div>
	</div>
</div>
<div class="footer">
	<div class="inner">
		<div class="copyright clearfix">
			<p class="copyright_en">Copyright &copy; 2012 - 2015 , 点击未来 All Rights Reserved.</p>
			<p class="copyright_ch">
				<a href="http://www.djie.net" target="_blank">http://www.djie.net</a>
			</p>
		</div>
	</div>
</div>
<script src="/dwz/Core/Org/common.js"></script>
<script>
function refreshs(){
	document.getElementById('code_img').src='<?php echo U("verify");?>?time='+Math.random();void(0);
}
$(function(){


	$.ajax({

		url:'<?php echo U("ajax_show_code");?>',
		dataType:'json',
		type:'post',
		success:function(res)
		{
			if(res==1)
				$('#verifytip').show();
			else
				$('#verifytip').hide();
		}
	})


	$('#verifycode').focus(function(){
		$('a.change_img').trigger("click");
	});

	$('#loginform').submit(function(){

		var data = $(this).serialize();
		var url  = $(this).attr('url');

		$.ajax({
			data:data,
			type:'post',
			dataType:'json',
			url:url,
			success:function(res){
				if(res.status==0)
				{
					resultTip({error:1,msg:res.info});
					if(res.show_code==1)
						$('#verifytip').show();
					
				}
				else
					location.href='<?php echo U('Index/index');?>';
			}

		})  
		return false;


	})



});
</script>
</body>
</html>