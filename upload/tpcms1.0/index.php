<?php
/**[入口文件]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-05-01 15:34:59
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-06-17 16:43:18
 */
if(!file_exists('Data/Config/db.inc.php')) header("location:install.php");
define('THINK_PATH','./Core/ThinkPHP/');
define('APP_NAME','Tpcms');
define('APP_PATH','./Core/Tpcms/');
define('RUNTIME_PATH',"./Data/Runtime/");
define('APP_DEBUG',true);
require THINK_PATH.'ThinkPHP.php';