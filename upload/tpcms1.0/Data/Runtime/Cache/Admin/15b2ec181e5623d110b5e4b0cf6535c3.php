<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>栏目列表</title>
	<script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Category'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Category/index';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
	<script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.base.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.category.js"></script>
	<link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/Css/mod.base.css" />
</head>
<body>
	<form action="<?php echo U('Category/beachdelete');?>" method="post" class='hd-form' name="operationForm">
		<div class="hd-menu-list">
			<ul>
				<li class='active'>
					<a href="javascript:;" >栏目列表</a>
				</li>
				<li>
					<a href="<?php echo U('Category/add');?>">添加栏目</a>
				</li>
				<li>
					<a href="<?php echo U('Category/update_cache');?>">更新栏目缓存</a>
				</li>
			</ul>
		</div>
		<table class="hd-table hd-table-list hd-form">
			<thead>
				<tr>
					<td class="hd-w30">
						<input type="checkbox" />
					</td>
					<td class="hd-w60">排序</td>
					<td class="hd-w30">CID</td>
					<td>栏目名称</td>
					<td>类型</td>
					<td>模型</td>
					<td>状态</td>
					<td class="hd-w150">操作</td>
				</tr>
			</thead>
			<?php if($category): if(is_array($category)): foreach($category as $key=>$v): ?><tr cid='<?php echo ($v["cid"]); ?>' <?php if($v['pid'] == 0): ?>class="top"<?php endif; ?>
					>
					<td>
						<input type="checkbox" name="cid[<?php echo ($v["cid"]); ?>]" value="<?php echo ($v["cid"]); ?>"/>
					</td>
					<td>
						<input type="text" class="hd-w30" value="<?php echo ($v["sort"]); ?>" name="sort[<?php echo ($v["cid"]); ?>]"/>
					</td>
					<td><?php echo ($v["cid"]); ?></td>
					
					<td>
						<?php if($v['pid'] == 0 && Third\Data::hasChild(S('category'),$v['cid'])): ?><img src="/dcms/Core/Tpcms/Admin/View/Public/images/contract.gif" action="2" class="explodeCategory hand"/><?php endif; ?>
						<?php if($v['pid'] == 0): ?><strong><?php echo ($v["_name"]); ?></strong>
							<?php else: ?>
							<?php echo ($v["_name"]); endif; ?>
					</td>
					<td><?php echo ($v["type"]); ?></td>
					<td><?php echo ($v["model"]); ?></td>
					<td><?php if($v["is_show"]): ?>显示<?php else: ?>不显示<?php endif; ?></td>
					<td>
						<!-- <a href="" target="_blank">访问</a>
					<span class="line">|</span>
					-->
					<a href="<?php echo U('Category/add',array('pid'=> $v['cid']));?>">
								添加子栏目
					</a>
					<span class="line">|</span>
					<a href="<?php echo U('Category/edit',array('cid'=> $v['cid']));?>">
								修改
					</a>
					<span class="line">|</span>
					<a href="javascript:;" onclick="del_modal('<?php echo U('Category/del',array('cid'=> $v['cid']));?>')">
								删除
					</a>
				</td>
			</tr><?php endforeach; endif; ?>
		<?php else: ?>
		<tr>
			<td colspan="7">没有找到符合的记录</td>
		</tr><?php endif; ?>
</table>

<input type="button" class="hd-btn hd-btn-sm select_all" value="全选">
<input type="button" class="hd-btn hd-btn-sm operation" value="排序" name="update_sort" >
</form>
<script type='tex/javascript'> 
var PUBLIC = '/dcms/Core/Tpcms/Admin/View/Public';
</script>
</body>
</html>