<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title><?php echo (L("lang_operation_success")); ?></title>
	<link rel="stylesheet" type="text/css" href="/dcms/Data/Public/css/css.css"/>
</head>
<body>

	<div class="wrap">
			<div class="title">
				<?php echo (L("lang_operation_success")); ?>
			</div>
			<div class="content">
				<div class="icon"></div>
				<div class="message">
					<div style="margin-top:10px;margin-bottom:15px;">
						<?php echo($message); ?>
					</div>
					<a href="<?php echo($jumpUrl); ?>" class="hd-cancel">
						<?php echo (L("lang_back")); ?>
					</a>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		window.onload=function(){
			window.setTimeout(function(){
				window.location.href='<?php echo($jumpUrl); ?>';
			},<?php echo($waitSecond); ?>*1000);
		
		}
		</script>
</body>
</html>