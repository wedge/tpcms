<?php
/**[字段表模型]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-15 22:06:38
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-02 00:08:24
 */
namespace Common\Service;
use Think\Model;
class ModelFieldService extends Model
{
	private $cache;
	public function _initialize()
	{
		$this->cache = S('modelField');
	}


	

	



	/**
	 * [get_all 读取对应模型所有字段]
	 * @param  [type] $mid [description]
	 * @return [type]      [description]
	 */
	public function get_all($mid)
	{	
		$data = isset($this->cache[$mid])?$this->cache[$mid]:null;
		if(!$data) return null;
		foreach($data as $k=>$v)
		{
			// 1 文本 ，2 多行文本 ，3 完整编辑器 ，4 简单编辑器 ，5 单选框 ，6 下拉框，7 多选框 ，8 文件上传框，9 图片上传框 ， 10 地区联动
			switch ($v['show_type']) 
			{
				case 1:
					$data[$k]['type'] = '文本';
					break;
				case 2:
					$data[$k]['type'] = '多行文本';
					break;
				case 3:
					$data[$k]['type'] = '编辑器';
					break;
				case 4:
					$data[$k]['type'] = '简单编辑器';
					break;
				case 5:
					$data[$k]['type'] = '单选框';
					break;
				case 6:
					$data[$k]['type'] = '下拉框';
					break;
				case 7:
					$data[$k]['type'] = '多选框';
					break;
				case 8:
					$data[$k]['type'] = '文件上传框';
					break;
				case 9:
					$data[$k]['type'] = '图片上传框';
					break;
			}
		}
		return $data;
	}

	

	
	/**
	 * [get_one 读取一个字段的信息]
	 * @return [type] [description]
	 */
	public function get_one($fid)
	{
		$mid = I('get.mid');
		$data = isset($this->cache[$mid][$fid])?$this->cache[$mid][$fid]:'';
		return $data;
	}


}