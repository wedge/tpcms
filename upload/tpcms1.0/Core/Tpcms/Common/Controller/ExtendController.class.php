<?php
/**[公用控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:34:56
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-29 16:52:15
 */
namespace Common\Controller;
use Think\Controller;
use Third\Code;
use Think\Upload;
class ExtendController extends Controller
{
	/**
   * [verify 验证码]
   * @return [type] [description]
   */
	public function verify()
	{
  	$code = new Code();
  	$code->show();
	}
  /**
   * [ajax_check_code 验证码比较]
   * @return [type] [description]
   */
  public function ajax_check_code()
  {
    if(!IS_AJAX)
      _404('非法操作');
    $code = I('post.code');
    $db = D('User',"Logic");
    $status = $db->check_code($code);
    if(!$status)
      $this->ajaxReturn(array('status'=>0,'message'=>'验证码错误'));
    else
      $this->ajaxReturn(array('status'=>1,'message'=>''));
  }

  /**
   * [keditor_upload 编辑器图片上传]
   * @return [type] [description]
   */
  public function keditor_upload()
  {
      // 上传类
      $upload = new Upload();             // 实例化上传类
      $upload->maxSize  = 314572800 ;     // 设置附件上传大小
      $upload->exts  = explode('|', C('cfg_file'));// 设置附件上传类型
      $upload->autoSub =false;            //不要自动创建子目录
      $upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
      $dir = I('get.dir');
      $upload->savePath = $dir.'/'.date('Y').'/'.date('m').'/'.date('d').'/';

      // 执行上传
      if(!$info=$upload->uploadOne($_FILES['imgFile']))
      {
        // 上传错误提示错误信息
        echo json_encode(array('error' => 1, 'message' =>"1".$upload->getError()));
      }
      else
      {

        // 上传成功 获取上传文件信息
        $fileUrl = $upload->rootPath.$info['savepath'].$info['savename'];
        $keditor = pathinfo($fileUrl);
        // 保存数据到数据库
        $data=array(
          'name'=>$keditor['filename'],
          'ext'=>$keditor['extension'],
          'path'=>$keditor['dirname'],
          'size'=>filesize($fileUrl),
          'addtime'=>time(),
          'user_uid'=>session('user_id'),
        );
        $id = D('Upload')->add($data);
        $_SESSION['keditor'][]=$id;
        $fullPath = __ROOT__ . '/' . $fileUrl;
        echo json_encode(array('error' => 0, 'url' => $fullPath));
      }
      exit;
  }

  /**
   * [down 下载]
   * @return [type] [description]
   */
  public function down()
  {
    $aid =I('get.aid');
    $field =I('get.field');

    // 附表名称
    $cid  = D('Article')->where(array('aid'=>$aid))->getField('category_cid');
    $cate =D('Category',"Service")->get_one($cid);
    if(!$cate)
      $this->error('链接错误');
    $mid = $cate['model_mid'];
    $model = D('Model','Service')->get_one($mid);
    if(!$model)
      $this->error('链接错误');
    $table= 'article_'.$model['name'];
    // 文件
    $db = D('ArticleView');
    
    $db->viewFields[$table] = array(
        '*',
        '_on'=>'article.aid='.$table.'.article_aid',
    );

    $file =  $db->where(array('article_aid'=>$aid))->getField($field);
    $file=iconv("utf-8","gb2312",$file);
    $fileName = basename($file);//获得文件名

    header("Content-type:application/octet-stream");//二进制文件
    header("Content-Disposition:attachment;filename={$fileName}");//下载窗口中显示的文件名
    header("Accept-ranges:bytes");//文件尺寸单位
    header("Accept-length:".filesize($file));//文件大小
    readfile($file);//读出文件内容
  }



  public function ajax_get_region()
  {
      $rid = I('post.rid');

      $db  = D('Region','Logic');
      $child = $db->get_child_region($rid);

      if(!$child)
        $this->ajaxReturn(array('status'=>0,'info'=>'信息不存在'));
      else
      {
        $str = '<option value="0">不限</option>';
        if($child)
        {
          foreach($child as $v)
          {
            $str .='<option value="'.$v['region_id'].'">'.$v['region_name'].'</option>';
          }
        }
        $this->ajaxReturn(array('status'=>1,'info'=>$str));
      }
  }


   public function ajax_code()
  {
    $code = I('get.code');
    if($code!=session('code'))
      echo "false";
    else
      echo "true";
    die;
  }
}