<?php
/** [编辑器上传模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 19:30:14
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-14 21:53:08
 */
namespace Admin\Logic;
use Think\Model;
class UploadLogic extends Model{
	
	/**
	 * [update_uoload_attachment 更新数据]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function update_uoload_attachment($aid)
	{
		//p($_SESSION['keditor']);
		// 必须有上传附件
		if(isset($_SESSION['keditor']))
		{
			$uploadModel = D('Upload');
			// 遍历session 更新关联
			foreach($_SESSION['keditor'] as $v)
			{
				$uploadModel->save(array('article_aid'=>$aid,'id'=>$v));
			}
		}
		// 清空
		unset($_SESSION['keditor']);
	}

	/**
	 * [get_all 读取所有的编辑器附件]
	 * @param  [type] $map         [description]
	 * @param  [type] $order       [description]
	 * @param  [type] $sort        [description]
	 * @param  [type] $currentPage [description]
	 * @param  [type] $listRows    [description]
	 * @return [type]              [description]
	 */
	public function get_all($map,$order,$sort,$currentPage,$listRows)
	{
		$data = D('UploadView')->where($map)->order($order.' '.$sort)->page($currentPage.','.$listRows)->select();
		return $data;
	}

	/**
	 * [del 删除]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function del($id)
	{
		$data  = $this->find($id);
		if(!$data)
		{
			$this->error='附件不存在';
			return false;
		}
		if($data['article_aid'])
		{
			$this->error='附件使用中';
			return false;
		}

		$file = $data['path'].'/'.$data['name'].'.'.$data['ext'];
		is_file($file) && unlink($file);

		$this->delete($id);

		return true;

	}

	/**
	 * [delete_upload_by_article_aid 删除附件通过关联外键article_aid]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function delete_upload_by_article_aid($aid)
	{
		$data = $this->where(array('article_aid'=>$aid))->select();
		if(!$data)
			return;
		foreach($data as $v)
		{
			$fullPath = $v['path'].'/'.$v['name'].'.'.$v['ext'];
			is_file($fullPath) && unlink($fullPath);
		}
		$this->where(array('article_aid'=>$aid))->delete();
	}



}