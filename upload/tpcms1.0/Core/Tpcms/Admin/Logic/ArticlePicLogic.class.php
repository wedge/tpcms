<?php
/** [图片集合模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-14 19:23:01
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-06-08 14:14:08
 */
namespace Admin\Logic;
use Think\Model;
use Think\Image;
use Think\Upload;
class ArticlePicLogic extends Model{


	public $small;
	public $medium;

	public function _initialize()
	{

		// 图集尺寸
		$this->small['width']   = C('cfg_pic_small_width');
		$this->small['height']  = C('cfg_pic_small_height');
		$this->medium['width']  = C('cfg_pic_medium_width');
		$this->medium['height'] = C('cfg_pic_medium_height');
	}

	/**
	 * [add_pic 添加图片集合]
	 * @param [type] $aid [description]
	 */
	public function add_pic($aid)
	{

		if(!empty($_FILES['img']['name']))
        { 
    
           
            $upload = new Upload();             // 实例化上传类
            $upload->maxSize  = 314572800 ;     // 设置附件上传大小
            $upload->exts  = explode('|', C('cfg_image'));// 设置附件上传类型
            $upload->autoSub =false;            //不要自动创建子目录
            $upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
            $upload->savePath = 'image/'.date('Y').'/'.date('m').'/'.date('d').'/';

            //$picTitle = $_POST['pic_title'];
        
           
            // is_dir($savePath) || mkdir($savePath,0777,true);
            if($info=$upload->upload(array($_FILES['img']))) 
            {   // 上传成功 获取上传文件信息
               

                // 获取比例
                $smallWidth = $this->small['width'];
                $smallHeight = $this->small['height'];
                $mediumWidth = $this->medium['width'];
                $mediumHeight = $this->medium['width'];

                $image = new Image(); 
                foreach($info as $k=> $f)
                {
                    // 定义路径
                    $f['path']=$upload->rootPath.$f['savepath'].$f['savename'];
                    $pic=pathinfo($f['path']);
                    // 缩略图
                    
                    $medium=$pic['dirname'] . '/' .$pic['filename']."_medium.".$pic['extension'];
                    $small = $pic['dirname'] . '/' .$pic['filename']."_small.".$pic['extension'];

                    $image->open($f['path']);
                    $image->thumb($mediumWidth, $mediumHeight,\Think\Image::IMAGE_THUMB_FILLED)->save($medium);
                   $image->thumb($smallWidth, $smallHeight,\Think\Image::IMAGE_THUMB_FILLED)->save($small);


                    // 添加
                    $this->add(array(
                        'article_aid'=> $aid,
                        'big'=>$f['path'],
                        'medium'=>$medium,
                        'small'=>$small,
                        //'pic_title'=>$picTitle[$k]
                    ));  
                }
            }

        }
	}

    public function add_attr_pic($aid)
    {

     
        $articleAttr = $_POST['article_attr'];
        $attrValueModel = D('AttrValue');
        $upload = new Upload();             // 实例化上传类
        $upload->maxSize  = 314572800 ;     // 设置附件上传大小
        $upload->exts  = explode('|', C('cfg_image'));// 设置附件上传类型
        $upload->autoSub =false;            //不要自动创建子目录
        $upload->rootPath = './Data/Uploads/'; //设置上传根路径 这个系统不会自动创建
        $upload->savePath = 'image/'.date('Y').'/'.date('m').'/'.date('d').'/';

        $image = new Image(); 

        foreach ($articleAttr as $k => $v) 
        {
             
            foreach ($v as  $value) 
            {
               $attrValueId = $attrValueModel->where(array('attr_value'=>$value))->getField('attr_value_id');
               if(!empty($_FILES[$attrValueId]['name']))
               {
            
                    $info=$upload->upload(array($_FILES[$attrValueId]));
                    if($info) 
                     {  
                        // 上传成功 获取上传文件信息


                        // 获取比例
                        $smallWidth = $this->small['width'];
                        $smallHeight = $this->small['height'];
                        $mediumWidth = $this->medium['width'];
                        $mediumHeight = $this->medium['width'];
                        foreach($info as $k=> $f)
                        {
                            // 定义路径
                            $f['path']=$upload->rootPath.$f['savepath'].$f['savename'];
                            $pic=pathinfo($f['path']);
                            // 缩略图
                            
                            $medium=$pic['dirname'] . '/' .$pic['filename']."_medium.".$pic['extension'];
                            $small = $pic['dirname'] . '/' .$pic['filename']."_small.".$pic['extension'];

                            $image->open($f['path']);
                            $image->thumb($mediumWidth, $mediumHeight,\Think\Image::IMAGE_THUMB_FILLED)->save($medium);
                            $image->thumb($smallWidth, $smallHeight,\Think\Image::IMAGE_THUMB_FILLED)->save($small);

                    
                            // 添加
                            $this->add(array(
                                'article_aid'=> $aid,
                                'big'=>$f['path'],
                                'medium'=>$medium,
                                'small'=>$small,
                                'attr_value_attr_value_id'=>$attrValueId
                                //'pic_title'=>$picTitle[$k]
                            )); 

                        }

                     }

               }
               

            }
        }

        //p($_FILES['img']['name']);die;
      
    }



	/**
	 * [get_all 获取图片]
	 * @return [type]      [description]
	 */
	public function get_all($aid)
	{
 		$data = $this->where(array('article_aid'=>$aid))->select();
        $attrValueModel = D('AttrValue');
 		if(!$data) return false;
        $isattr = 0;
 		// 组合路径
 		foreach ($data as $k=>$v)
 		{
          

            if($v['attr_value_attr_value_id'])
            {
                $isattr = 1;

                $temp[$v['attr_value_attr_value_id']]['attr_value_name'] = $attrValueModel->where(array('attr_value_id'=>$v['attr_value_attr_value_id']))->getField('attr_value_name');
                $temp[$v['attr_value_attr_value_id']]['attr_value_attr_value_id']=$v['attr_value_attr_value_id'];
                $temp[$v['attr_value_attr_value_id']]['pics'][$k]['small'] = __ROOT__.'/'.$v['small'];
                $temp[$v['attr_value_attr_value_id']]['pics'][$k]['medium'] = __ROOT__.'/'.$v['medium'];
                $temp[$v['attr_value_attr_value_id']]['pics'][$k]['big'] = __ROOT__.'/'.$v['big'];
                $temp[$v['attr_value_attr_value_id']]['pics'][$k]['article_aid'] = $v['article_aid'];
                $temp[$v['attr_value_attr_value_id']]['pics'][$k]['id'] = $v['id'];
            }
            else
            {
                $temp[$v['attr_value_attr_value_id']]['article_aid'] = $v['article_aid'];
                $temp[$v['attr_value_attr_value_id']]['id'] = $v['id'];
                $temp[$k]['small'] = __ROOT__.'/'.$v['small'];
                $temp[$k]['medium'] = __ROOT__.'/'.$v['medium'];
                $temp[$k]['big'] = __ROOT__.'/'.$v['big'];
            }
 			
 		}

 		return array('isattr'=>$isattr,'pics'=>$temp);
	}
    /**
     * [del_pic 删除图片]
     * @return [type] [description]
     */
    public function del_pic()
    {
        $id  = I('get.id');
        $aid = I('get.aid');
        $data = $this->where(array('article_aid'=>$aid,'id'=>$id))->find();
        if(!$data)
        {
            $this->error = '链接错误';
            return false;
        }

        // 删除图片
        is_file($data['small'])  && unlink($data['small']);
        is_file($data['medium']) && unlink($data['medium']);
        is_file($data['big'])    && unlink($data['big']);

        $this->delete($id);


        return true;

    }

    public function del_attr_pic()
    {
        $attrValueId  = I('get.attrvalueid');
        $aid = I('get.aid');
        $data = $this->where(array('article_aid'=>$aid,'attr_value_attr_value_id'=>$attrValueId))->select();
        if(!$data)
        {
            $this->error = '链接错误';
            return false;
        }
        foreach($data as $v)
        {
            // 删除图片
            is_file($v['small'])  && unlink($v['small']);
            is_file($v['medium']) && unlink($v['medium']);
            is_file($v['big'])    && unlink($v['big']);

            $this->delete($v['id']);
        }
       


        return true;
    }

   
    /**
     * [delete_pic_by_article_aid 删除图片]
     * @param  [type] $aid [description]
     * @return [type]      [description]
     */
    public function delete_pic_by_article_aid($aid)
    {
        $data = $this->where(array('article_aid'=>$aid))->select();
        if($data)
        {
            foreach($data as $v)
            {
                // 删除图片
                is_file($v['small'])  && unlink($v['small']);
                is_file($v['medium']) && unlink($v['medium']);
                is_file($v['big'])    && unlink($v['big']);
                $this->delete($v['id']);
            }
        }
    }

   
}